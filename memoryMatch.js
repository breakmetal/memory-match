var clickedCard = []
var errors=0
var couples = 0
var clock
var timeController
var time=0
setUp();
function setUp(){
    clock=document.getElementById("clock")
    timeController=setInterval(functionClock,1000)
    var grid = document.getElementsByTagName("td");
    var answers = randomAnswers();
    for(var i = 0; i < grid.length; i++){
        let cell
        cell = grid[i];
        cell.children[1].innerHTML=answers[i].route
        cell.completed = false;
        cell.clicked = false;
        cell.value=answers[i].value
        cell.addEventListener("click",function(){flipCard(this)});
    }
}
function flipCard(card){
    if(card.completed == false && card.clicked == false && clickedCard.length<2){
        card.style.transform = "rotateY(180deg)";
        card.clicked=true;
        clickedCard.push(card)
    }
    setTimeout(completed,1250)
}
function completed(){
    if(clickedCard.length==2){
        if(clickedCard[0].value==clickedCard[1].value){
            clickedCard[0].completed=true
            clickedCard[1].completed=true
            couples++
            gameOver()
        }else{
            clickedCard[0].style.transform = "rotateY(360deg)"
            clickedCard[1].style.transform = "rotateY(360deg)"
            clickedCard[0].clicked=false
            clickedCard[1].clicked=false
            errors++
        }
        clickedCard=[]
    }
}
function randomAnswers(){
    var answers = data.images;
    answers.sort(function(item){
        return .7 - Math.random();
    })
    return answers;
}
function gameOver(){
    console.log(couples)
    if(couples===6){
        clearInterval(timeController)
        alert(`congratulation your time is ${time} seconds and you committed ${errors} errors`)

    }
}
function functionClock(){
    time++
    clock.innerHTML=`Time:${time}`

}

// Vectores Gratis hechos por <a rel="nofollow" href="https://es.vecteezy.com">https://es.vecteezy.com</a> 